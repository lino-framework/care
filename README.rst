=========================
The ``lino-care`` package
=========================




Lino Care helps you to manage a network of people who care.

- Source code: https://gitlab.com/lino-framework/care

- Technical documentation, including demo projects, API and tested
  specs see https://dev.lino-framework.org/specs/care.html

- For *introductions* and *commercial information* about Lino Care
  please see https://www.saffre-rumma.net



