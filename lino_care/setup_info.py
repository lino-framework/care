# -*- coding: UTF-8 -*-
# Copyright 2014-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

# $ python setup.py test -s tests.PackagesTests.test_packages

SETUP_INFO = dict(
    name='lino-care',
    version='20.1.0',
    install_requires=['lino-xl'],
    # tests_require=['pytest', 'mock'],
    test_suite='tests',
    description=(
        "Manage a network of helpers."),
    long_description="""\

Lino Care helps you to manage a network of people who care.

- Source code: https://gitlab.com/lino-framework/care

- Technical documentation, including demo projects, API and tested
  specs see https://dev.lino-framework.org/specs/care.html

- For *introductions* and *commercial information* about Lino Care
  please see https://www.saffre-rumma.net


""",
    author='Rumma & Ko Ltd',
    author_email='info@lino-framework.org',
    url="https://gitlab.com/lino-framework/care",
    license_files=['COPYING'],
    classifiers="""\
Programming Language :: Python
Programming Language :: Python :: 2
Development Status :: 4 - Beta
Environment :: Web Environment
Framework :: Django
Intended Audience :: Developers
Intended Audience :: System Administrators
Intended Audience :: Information Technology
Intended Audience :: Customer Service
License :: OSI Approved :: GNU Affero General Public License v3
Operating System :: OS Independent
Topic :: Software Development :: Bug Tracking
""".splitlines())

SETUP_INFO.update(packages=[
    str(n) for n in """
lino_care
lino_care.lib
lino_care.lib.care
lino_care.lib.care.fixtures
lino_care.lib.contacts
lino_care.lib.contacts.fixtures
lino_care.lib.public
lino_care.lib.users
lino_care.lib.users.fixtures
lino_care.lib.cal
lino_care.lib.cal.fixtures
lino_care.lib.courses
lino_care.lib.tickets
""".splitlines() if n
])

SETUP_INFO.update(include_package_data=True, zip_safe=False)
